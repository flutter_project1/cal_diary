import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ExerciseWidget extends StatefulWidget {
  const ExerciseWidget({Key? key}) : super(key: key);

  @override
  _ExerciseWidgetState createState() => _ExerciseWidgetState();
}

class _ExerciseWidgetState extends State<ExerciseWidget> {
  var exercises = [
    'cardio 1 hour (455 cal)',
    'dancing 1 hour (300 cal)',
    'hula hooping 1 hour (400 cal)',
    'swimming 1 hour (400 cal)',
    'upper body exercises 1 set (200 cal)',
    'lower body exercises 1 set (200 cal)',
    'back exercises 1 set (200 cal)',
    'core exercises 1 set (200 cal)'
  ];
  var exerciseValues = [false, false, false, false, false, false, false, false];
  var exerciseCal = [455, 300, 400, 400, 200, 200, 200, 200];
  var burnCal = 0;

  @override
  void initState() {
    super.initState();
    _loadExercise();
  }

  Future<void> _loadExercise() async {
    var prefs = await SharedPreferences.getInstance();
    var strExValue = prefs.getString('exercise_values') ??
        '[false, false, false, false, false, false, false, false]';
    // print(strExValue.substring(1, strExValue.length - 1));
    var arrStrExValues =
        strExValue.substring(1, strExValue.length - 1).split(',');

    setState(() {
      burnCal = prefs.getInt('burnCal') ?? 0;
      for (var i = 0; i < arrStrExValues.length; i++) {
        exerciseValues[i] = (arrStrExValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveExercise() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setInt('burnCal', burnCal);
      prefs.setString('exercise_values', exerciseValues.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Exercise'),
        // backgroundColor: Color(0xFF3e76a3),
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.local_fire_department,
                  color: Color(0xFFff550d),
                ),
                Center(
                    child: Text(
                  " $burnCal calories",
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                )),
              ],
            ),
            CheckboxListTile(
                value: exerciseValues[0],
                title: Text(exercises[0]),
                onChanged: (newValue) {
                  setState(() {
                    (newValue == true)
                        ? burnCal += exerciseCal[0]
                        : burnCal -= exerciseCal[0];

                    exerciseValues[0] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: exerciseValues[1],
                title: Text(exercises[1]),
                onChanged: (newValue) {
                  setState(() {
                    (newValue == true)
                        ? burnCal += exerciseCal[1]
                        : burnCal -= exerciseCal[1];
                    exerciseValues[1] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: exerciseValues[2],
                title: Text(exercises[2]),
                onChanged: (newValue) {
                  setState(() {
                    (newValue == true)
                        ? burnCal += exerciseCal[2]
                        : burnCal -= exerciseCal[2];
                    exerciseValues[2] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: exerciseValues[3],
                title: Text(exercises[3]),
                onChanged: (newValue) {
                  setState(() {
                    (newValue == true)
                        ? burnCal += exerciseCal[3]
                        : burnCal -= exerciseCal[3];
                    exerciseValues[3] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: exerciseValues[4],
                title: Text(exercises[4]),
                onChanged: (newValue) {
                  setState(() {
                    (newValue == true)
                        ? burnCal += exerciseCal[4]
                        : burnCal -= exerciseCal[4];
                    exerciseValues[4] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: exerciseValues[5],
                title: Text(exercises[5]),
                onChanged: (newValue) {
                  setState(() {
                    (newValue == true)
                        ? burnCal += exerciseCal[5]
                        : burnCal -= exerciseCal[5];
                    exerciseValues[5] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: exerciseValues[6],
                title: Text(exercises[6]),
                onChanged: (newValue) {
                  setState(() {
                    (newValue == true)
                        ? burnCal += exerciseCal[6]
                        : burnCal -= exerciseCal[6];
                    exerciseValues[6] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: exerciseValues[7],
                title: Text(exercises[7]),
                onChanged: (newValue) {
                  setState(() {
                    (newValue == true)
                        ? burnCal += exerciseCal[7]
                        : burnCal -= exerciseCal[7];
                    exerciseValues[7] = newValue!;
                  });
                }),
            Container(padding: EdgeInsets.all(16)),
            ElevatedButton(
              onPressed: () {
                _saveExercise();
                Navigator.pop(context);
              },
              child: const Text('SAVE'),
            ),
            Container(
              margin: EdgeInsets.all(8.0),
            ),
            ElevatedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.red)),
              onPressed: () {
                _clearExercise();
                // Navigator.pop(context);
              },
              child: const Text('CLEAR'),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _clearExercise() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('burnCal');
    prefs.remove('exercise_values');
    await _loadExercise();
  }
}
