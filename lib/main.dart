import 'package:cal_diary/exercise.dart';
import 'package:cal_diary/profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    title: 'Cal Diary',
    home: MyApp(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  //Main
  int kcal = 0;
  double bmrM = 0;
  double bmrF = 0;
  double bmi = 0;
  double goalWater = 0;

  // Profile
  String name = "name";
  String gender = "-"; //M,F
  int age = 0;
  double height = 0.0;
  double weight = 0.0;

  //Exercise
  int burnCal = 0;

  //Water
  double drinkWater = 0;

  @override
  void initState() {
    super.initState();
    _loadProfile();
    _loadExercise();
    // _loadDrinkWater();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      gender = prefs.getString('gender') ?? '-';
      age = prefs.getInt('age') ?? 0;
      height = prefs.getDouble('height') ?? 0.0;
      weight = prefs.getDouble('weight') ?? 0.0;
      bmi = ((weight) / (height / 100 * height / 100));
      bmrM = (66 + (13.7 * weight) + (5 * height) - (6.8 * age));
      bmrF = (665 + (9.6 * weight) + (1.8 * height) - (4.7 * age));
      goalWater = weight * 2.2 * 30 / 2;
      drinkWater = prefs.getDouble('drink_water') ?? 0;
    });
  }

  Future<void> _loadExercise() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      burnCal = prefs.getInt('burnCal') ?? 0;
      kcal = burnCal;
    });
  }

  Future<void> _saveDrinkWater() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setDouble('drink_water', drinkWater);
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('gender');
    prefs.remove('age');
    prefs.remove('height');
    prefs.remove('weight');
    prefs.remove('burnCal');
    prefs.remove('exercise_values');
    prefs.remove('drink_water');
    await _loadProfile();
    await _loadExercise();
    // await _loadDrinkWater();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Main'),
        // backgroundColor: Color(0xFF3e76a3),
      ),
      body: ListView(
        children: [
          Container(
            decoration: new BoxDecoration(
              boxShadow: [
                new BoxShadow(
                  color: Colors.grey.withOpacity(.5),
                  blurRadius: 20.0,
                ),
              ],
            ),
            child: Card(
              clipBehavior: Clip.antiAlias,
              color: Color(0xFFf4f4f4),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20))),
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.person),
                    title: Text('$name  gender: $gender  age: $age'),
                    subtitle: Text(
                      'height: $height cm. weight: $weight kg.',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.all(20.0),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Text(
                                        'BMI',
                                        style: TextStyle(
                                            fontSize: 30.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Container(
                                        child: (bmi.isNaN)
                                            ? Text(
                                                '-',
                                                style:
                                                    TextStyle(fontSize: 20.0),
                                              )
                                            : Text('${bmi.toStringAsFixed(2)}',
                                                style: TextStyle(
                                                    fontSize: 20.0,
                                                    fontWeight: (!bmi.isNaN)
                                                        ? FontWeight.bold
                                                        : FontWeight.normal,
                                                    color: (bmi.isNaN)
                                                        ? Color(0xFF000000)
                                                        : (bmi >= 30 ||
                                                                bmi <= 18.5)
                                                            ? Color(0xFFcc0000)
                                                            : (bmi >= 25 &&
                                                                    bmi <= 29.9)
                                                                ? Color(
                                                                    0xFFedaf1f)
                                                                : Color(
                                                                    0xFF298c29))))
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.all(20.0),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Text('BMR',
                                          style: TextStyle(
                                              fontSize: 30.0,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                    Container(
                                        child: (gender == '-')
                                            ? Text(
                                                '-',
                                                style:
                                                    TextStyle(fontSize: 20.0),
                                              )
                                            : (gender == "female")
                                                ? Text(
                                                    '${bmrF.toStringAsFixed(0)}',
                                                    style: TextStyle(
                                                        fontSize: 21.0,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  )
                                                : Text(
                                                    '${bmrM.toStringAsFixed(0)}',
                                                    style: TextStyle(
                                                        fontSize: 21.0,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ))
                                  ],
                                ),
                              )
                            ],
                          ),
                          Column(
                            children: [
                              Center(
                                child: Text(
                                    'Daily goal drink water (${goalWater.toStringAsFixed(0)} ml.)',
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold)),
                              ),
                              Center(
                                  child: ((drinkWater / (goalWater / 100)).isNaN
                                      ? Text('-',
                                          style: TextStyle(
                                            fontSize: 20.0,
                                          ))
                                      : ((drinkWater / (goalWater / 100))
                                                  .ceilToDouble() >=
                                              100)
                                          ? Text(
                                              '${(drinkWater / (goalWater / 100)).toStringAsFixed(0)} %',
                                              style: TextStyle(
                                                  fontSize: 20.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: Color(0xFF2aa4eb)))
                                          : Text(
                                              '${(drinkWater / (goalWater / 100)).toStringAsFixed(0)} %',
                                              style: TextStyle(
                                                  fontSize: 20.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: Color(0xFFcc0000)))))
                            ],
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            'Today',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                          Container(
                            margin: const EdgeInsets.all(15.0),
                            padding: const EdgeInsets.all(3.0),
                            decoration: BoxDecoration(
                                // color: Color(0xFFb9e0a5),
                                border: Border.all(
                                    color: Color(0xFFf5f5f5), width: 5),
                                borderRadius: BorderRadius.circular(20.0)),
                            child: Center(
                                child: Text(
                              '$kcal cal',
                              style: TextStyle(fontSize: 35.0),
                            )),
                          )
                        ],
                      )
                    ],
                  ),
                  Container(
                    child: ButtonBar(
                      alignment: MainAxisAlignment.end,
                      children: [
                        TextButton(
                            onPressed: () async {
                              await _resetProfile();
                            },
                            child: Text('Reset')),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.person_add_alt),
            title: Text('Profile'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProfileWidget()));
              await _loadProfile();
            },
          ),
          ListTile(
            leading: Icon(Icons.fastfood_outlined),
            title: Text('Meal'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.fitness_center_outlined),
            title: Text(
              'Exercise',
            ),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ExerciseWidget()));
              await _loadExercise();
            },
          ),

          Container(
            height: 20,
          ),
          //Drink water feature
          Container(
            color: Color(0xFFc3e6fa).withOpacity(0.25),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.local_drink_rounded,
                      color: Color(0xFF2aa4eb),
                    ),
                    Text(
                        ' Daily goal drink water (${goalWater.toStringAsFixed(0)} ml.)',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(left: 20.0, right: 20.0),
                  child: Column(
                    children: [
                      Slider(
                        value: drinkWater,
                        onChanged: (newValue) {
                          setState(() {
                            drinkWater = newValue;
                            _saveDrinkWater();
                          });
                        },
                        min: 0,
                        max: goalWater.round() as double,
                        // max: goalWater.ceilToDouble(),
                        divisions: 20,
                        label: "${drinkWater.toStringAsFixed(0)}",
                      ),
                      Row(
                        children: [
                          Text('0',
                              style: TextStyle(
                                fontSize: 14,
                              )),
                          Spacer(),
                          Text('${goalWater.toStringAsFixed(0)}',
                              style: TextStyle(
                                fontSize: 14,
                              ))
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
