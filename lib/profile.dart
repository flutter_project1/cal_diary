import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileWidget extends StatefulWidget {
  const ProfileWidget({Key? key}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final _formKey = GlobalKey<FormState>();
  String name = "";
  String gender = "-"; //Male,Female
  int age = 0;
  double height = 0.0;
  double weight = 0.0;

  final _nameController = TextEditingController();
  final _ageController = TextEditingController();
  final _heightController = TextEditingController();
  final _weightController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      _nameController.text = name;
      gender = prefs.getString('gender') ?? '-';
      age = prefs.getInt('age') ?? 0;
      _ageController.text = '$age';
      height = prefs.getDouble('height') ?? 0.0;
      _heightController.text = '$height';
      weight = prefs.getDouble('weight') ?? 0.0;
      _weightController.text = '$weight';
    });
  }

  Future<void> _saveProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('name', name);
      prefs.setString('gender', gender);
      prefs.setInt('age', age);
      prefs.setDouble('height', height);
      prefs.setDouble(('weight'), weight);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
        // backgroundColor: Color(0xFF3e76a3),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              // Text('$name $gender $age $height $weight'),
              TextFormField(
                autofocus: true,
                controller: _nameController,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 5) {
                    return 'please input your name';
                  }
                  return null;
                },
                // autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'Enter your name'),
                onChanged: (value) {
                  setState(() {
                    name = value;
                  });
                },
              ),
              DropdownButtonFormField(
                value: gender,
                validator: (value) {
                  if (value == '-') {
                    return 'please select you gender';
                  }
                  return null;
                },
                items: [
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Text('Select your gender'),
                      ],
                    ),
                    value: '-',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.male),
                        Text('Male'),
                      ],
                    ),
                    value: 'male',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.female),
                        Text('Female'),
                      ],
                    ),
                    value: 'female',
                  ),
                ],
                onChanged: (String? newValue) {
                  setState(() {
                    gender = newValue!;
                  });
                },
              ),
              TextFormField(
                controller: _ageController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || int.parse(value) < 20) {
                    return 'please input your age greater than or equal to 20 years';
                  }
                  return null;
                },
                //autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'Enter your age'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    age = int.tryParse(value)!;
                  });
                },
              ),
              TextFormField(
                controller: _heightController,
                validator: (value) {
                  var num = double.tryParse(value!);
                  if (num == null || double.parse(value) <= 0) {
                    return 'please input your height (cm.)';
                  }
                  return null;
                },
                //autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration:
                    InputDecoration(labelText: 'Enter your height (cm.)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    height = double.tryParse(value)!;
                  });
                },
              ),
              TextFormField(
                controller: _weightController,
                validator: (value) {
                  var num = double.tryParse(value!);
                  if (num == null || double.parse(value) <= 0) {
                    return 'please input your weight (kg.)';
                  }
                  return null;
                },
                //autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration:
                    InputDecoration(labelText: 'Enter your weight (kg.)'),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    weight = double.tryParse(value)!;
                  });
                },
              ),
              Container(padding: EdgeInsets.all(16)),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    _saveProfile();
                    Navigator.pop(context);
                  }
                },
                child: const Text('SAVE'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
